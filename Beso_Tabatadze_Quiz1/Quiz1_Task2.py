"""
განსაზღვრეთ მდებარეობს თუ არა A(a; b) წერტილი R რადიუსიანი წრეწირის
შიგნით, a,b და R შეიტანეთ კლავიატურიდან,
თუ წრეწირის ცენტრი კოორდინატთა სათავეზე მდებარეობს.
"""
import math

# print(pow(4, 3))
# print(3.8**3)

a = float(input("a=>"))
b = float(input("b=>"))
R = float(input("R=>"))

l = math.sqrt(pow(a, 2)+pow(b, 2))

if R > 0:
    if l <= R:
        print("True")
    else:
        print("False")
else:
    print("R < = 0")



