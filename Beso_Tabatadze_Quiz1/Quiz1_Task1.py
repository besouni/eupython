"""
    შეიტანეთ სამი რიცხვი,განსაზღვრეთ გამოდგება თუ არა ისინი სამკუთხედის გვერდების ზომებად.
    თუ გამოდგება გამოიტანეთ ეკრანზე შეტყობინება „True“,
    წინააღმდეგ შემთხვევაში გამოიტანეთ შეტყობინება „False“.
"""
import math

print("Quiz1_Task1")
# print(math.sqrt(5))
# x = 5
# print(x)  # 5
# x = x + 7
# print(x)  # 12
# x += 3   # x = x + 3
# print(x) # 15
# x /= 5
# print(x) # 3


a = float(input("a=>"))
b = float(input("b=>"))
c = float(input("c=>"))
if a >= 0 and b >= 0 and c >= 0 and a + b > c and a + c > b and b + c > a:
    # print("True")
    p = (a+b+c)/2
    s = p*(p-a)*(p-b)*(p-c)
    s = math.sqrt(s)
    print(s)
else:
    print("False")
